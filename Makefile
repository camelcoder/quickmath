CFLAGS := -Wall -Wextra -pedantic -Wno-unused-function -lm

test: *.h *.c
	c89 $(CFLAGS) test.c -o $@

run: test
	./test

clean:
	rm test

.PHONY: run clean
