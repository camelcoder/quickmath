#define QUICK_MATH_IMPLEMENT
#include "quickmath.h"
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	QMvec3 x = {10, 3, 2.7172};
	QMvec3 y = {2, 3, 4};
	QMvec3 z;
	z = qm_cross_v3(x, y);
	printf("%f, %f, %f\n", z.x, z.y, z.z);

	return 0;
}
