
#if !defined(QUICK_MATH_H)

/**
 * Language detection
 **/
#define QM_STDC_C11 (__STDC_VERSION__ >= 201112L)
#define QM_STDC_C99 (__STDC_VERSION__ >= 199901L)

/**
 * Standart includes
 **/
#if !defined(qm_sin)   || !defined(qm_cos)  || !defined(qm_tan) || \
    !defined(qm_asin)  || !defined(qm_acos) || !defined(qm_atan) || \
    !defined(qm_atan2) || !defined(qm_sqrt)
	#include <math.h>
#endif

/**
 * Function scope
 **/
#if QM_STDC_C99
	#define QM_INLINE static inline
#else
	#define QM_INLINE static
#endif
#define QM_EXTERN extern

/**
 * Constants
 **/
#define QM_PI 3.1415926535897932384626433832795028841971693993751058209749L

/**
 * Trivial types
 **/
#if defined(QM_TYPES_OPENGL)
typedef GLboolean QMbool;
typedef GLint     QMint;
typedef GLuint    QMuint;
typedef GLfloat   QMfloat;
typedef GLdouble  QMdouble;
typedef GLbyte  QMbyte;
typedef GLubyte QMubyte;
#elif !defined(QM_TYPES_CUSTOM)
typedef unsigned char QMbool;
typedef int           QMint;
typedef unsigned int  QMuint;
typedef float         QMfloat;
typedef double        QMdouble;
typedef signed char   QMbyte;
typedef unsigned char QMubyte;
#endif

/**
 * Vectors
 **/
#define QM_GEN_TYPES(type, prefix, vec) \
	typedef struct { type x, y; } prefix##vec2;       \
	typedef struct { type x, y, z; } prefix##vec3;    \
	typedef struct { type x, y, z, w; } prefix##vec4; \
\
	/* Initialisation */                                     \
	prefix##vec2 qm_##vec##2_fill(const type v) {            \
		prefix##vec2 ret;                                \
		ret.x = ret.y = v;                               \
		return ret;                                      \
	}                                                        \
	prefix##vec2 qm_##vec##2(const type vx, const type vy) { \
		prefix##vec2 ret;                                \
		ret.x = vx; ret.y = vy;                          \
		return ret;                                      \
	}                                                        \
	prefix##vec3 qm_##vec##3_fill(const type v) {            \
		prefix##vec3 ret;                                \
		ret.x = ret.y = ret.z = v;                       \
		return ret;                                      \
	}                                                        \
	prefix##vec3 qm_##vec##3(const type vx, const type vy,   \
	                         const type vz) {                \
		prefix##vec3 ret;                                \
		ret.x = vx; ret.y = vy; ret.z = vz;              \
		return ret;                                      \
	}                                                        \
	prefix##vec4 qm_##vec##4_fill(const type v) {            \
		prefix##vec4 ret;                                \
		ret.x = ret.y = ret.z = ret.w = v;               \
		return ret;                                      \
	}                                                        \
	prefix##vec4 qm_##vec##4(const type vx, const type vy,   \
	                         const type vz, const type vw) { \
		prefix##vec4 ret;                                \
		ret.x = vx; ret.y = vy; ret.z = vz; ret.w = vw;  \
		return ret;                                      \
	}                                                        \

QM_GEN_TYPES(QMbool, QMb, bvec)
QM_GEN_TYPES(QMint, QMi, ivec)
QM_GEN_TYPES(QMuint, QMu, uvec)
QM_GEN_TYPES(QMfloat, QM, vec)
QM_GEN_TYPES(QMdouble, QMd, dvec)

#undef QM_GEN_TYPES
#undef QM_GEN_INITS
#undef QM_GEN_STRUCTS

/**
 * Matrices
 **/
#define QM_GEN_TYPES(type, prefix, mat)  \
	typedef struct {                 \
		type m00, m01;           \
		type m10, m11;           \
	} prefix##mat2;                  \
	typedef struct {                 \
		type m00, m01, m02;      \
		type m10, m11, m12;      \
		type m20, m21, m22;      \
	} prefix##mat3;                  \
	typedef struct {                 \
		type m00, m01, m02, m03; \
		type m10, m11, m12, m13; \
		type m20, m21, m22, m23; \
		type m30, m31, m32, m33; \
	} prefix##mat4;                  \
\
	/* Initialization */                               \
	QM_INLINE prefix##mat2 qm_##mat##2(void) {         \
		const prefix##mat2 ret = {0};              \
		return ret;                                \
	}                                                  \
	QM_INLINE prefix##mat3 qm_##mat##3(void) {         \
		const prefix##mat3 ret = {0};              \
		return ret;                                \
	}                                                  \
	QM_INLINE prefix##mat4 qm_##mat##4(void) {         \
		const prefix##mat4 ret = {0};              \
		return ret;                                \
	}                                                  \
	QM_INLINE prefix##mat2 qm_##mat##2_diag(type v) {  \
		prefix##mat2 ret = {0};                    \
		ret.m00 = ret.m11 = v;                     \
		return ret;                                \
	}                                                  \
	QM_INLINE prefix##mat3 qm_##mat##3_diag(type v) {  \
		prefix##mat3 ret = {0};                    \
		ret.m00 = ret.m11 = ret.m22 = v;           \
		return ret;                                \
	}                                                  \
	QM_INLINE prefix##mat4 qm_##mat##4_diag(type v) {  \
		prefix##mat4 ret = {0};                    \
		ret.m00 = ret.m11 = ret.m22 = ret.m33 = v; \
		return ret;                                \
	}

QM_GEN_TYPES(QMfloat, QM, mat)
QM_GEN_TYPES(QMdouble, QMd, dmat)

#undef QM_GEN_TYPES
#undef QM_GEN_INITS
#undef QM_GEN_STRUCTS

/**
 * Math functions
 **/
#if !defined(qm_sin)
	#define qm_sin sin
#endif
#if !defined(qm_cos)
	#define qm_cos cos
#endif
#if !defined(qm_tan)
	#define qm_tan tan
#endif
#if !defined(qm_asin)
	#define qm_asin asin
#endif
#if !defined(qm_acos)
	#define qm_acos acos
#endif
#if !defined(qm_atan)
	#define qm_atan atan
#endif
#if !defined(qm_atan2)
	#define qm_atan2 atan2
#endif
#if !defined(qm_sqrt)
	#define qm_sqrt sqrt
#endif

/**
 * Utility functions
 **/
#define qm_f_to_radians(degrees) (degrees * (QMfloat)(QM_PI / 180.0))
#define qm_d_to_radians(degrees) (degrees * (QMdouble)(QM_PI / 180.0))
QM_INLINE QMfloat m3d_lerpf(QMfloat a, QMfloat n, QMfloat b) {
	return a + (b - a) * n;
}
QM_INLINE QMdouble m3d_lerpd(QMdouble a, QMdouble n, QMdouble b) {
	return a + (b - a) * n;
}

/**
 * Matrix functions
 **/
QM_INLINE QMmat4 qm_orthographic(QMfloat left, QMfloat right,
                                 QMfloat bottom, QMfloat top,
                                 QMfloat near, QMfloat far)
{
	QMmat4 ret = {0};
	ret.m00 = 2 / (right - left);
	ret.m11 = 2 / (top - bottom);
	ret.m22 = 2 / (near - far);
	ret.m33 = 1;

	ret.m30 = (left + right) / (left - right);
	ret.m31 = (bottom + top) / (bottom - top);
	ret.m32 = (far + near) / (near - far);
	return ret;
}
QM_INLINE QMmat4 qm_perspective(QMfloat fov, QMfloat aspect,
                                QMfloat near, QMfloat far)
{
	const QMfloat tanfov = qm_tan(fov * (float)(QM_PI / 360.0));
	QMmat4 ret = {0};
	ret.m00 = 1.0f / tanfov;
	ret.m11 = aspect / tanfov;
	ret.m23 = -1.0f;
	ret.m22 = (near + far) / (near - far);
	ret.m32 = (2.0f * near * far) / (near - far);
	ret.m33 = 0.0f;
	return ret;
}

/* Movement */
QM_INLINE QMmat4 qm_translate(QMvec3 translation)
{
	QMmat4 ret = {0};
	ret.m00 = 1;
	ret.m11 = 1;
	ret.m22 = 1;
	ret.m33 = 1;
	ret.m30 = translation.x;
	ret.m31 = translation.y;
	ret.m32 = translation.z;
	return ret;
}
QM_INLINE QMmat4 qm_scale(QMvec3 scale)
{
	QMmat4 ret = {0};
	ret.m00 = scale.x;
	ret.m11 = scale.y;
	ret.m22 = scale.z;
	ret.m33 = 1;
	return ret;
}

QM_EXTERN QMmat4 qm_rotate(QMfloat angle, QMvec3 axis);
QM_EXTERN QMmat4 qm_lookat(QMvec3 eye, QMvec3 center, QMvec3 up);

/**
 * Vector binary operations
 **/
/* Addition */
QM_INLINE QMvec2 qm_add_v2f(QMvec2 l, QMfloat r) {
	l.x += r; l.y += r; return l;
}
QM_INLINE QMvec2 qm_add_v2(QMvec2 l, QMvec2 r) {
	l.x += r.x; l.y += r.y; return l;
}
QM_INLINE QMvec3 qm_add_v3f(QMvec3 l, QMfloat r) {
	l.x += r; l.y += r; l.z += r; return l;
}
QM_INLINE QMvec3 qm_add_v3(QMvec3 l, QMvec3 r) {
	l.x += r.x; l.y += r.y; l.z += r.z; return l;
}
QM_INLINE QMvec4 qm_add_v4f(QMvec4 l, QMfloat r) {
	l.x += r; l.y += r; l.z += r; l.w += r; return l;
}
QM_INLINE QMvec4 qm_add_v4(QMvec4 l, QMvec4 r) {
	l.x += r.x; l.y += r.y; l.z += r.z; l.w += r.w; return l;
}

/* Substraction */
QM_INLINE QMvec2 qm_sub_v2f(QMvec2 l, QMfloat r) {
	l.x -= r; l.y -= r; return l;
}
QM_INLINE QMvec2 qm_sub_v2(QMvec2 l, QMvec2 r) {
	l.x -= r.x; l.y -= r.y; return l;
}
QM_INLINE QMvec3 qm_sub_v3f(QMvec3 l, QMfloat r) {
	l.x -= r; l.y -= r; l.z -= r; return l;
}
QM_INLINE QMvec3 qm_sub_v3(QMvec3 l, QMvec3 r) {
	l.x -= r.x; l.y -= r.y; l.z -= r.z; return l;
}
QM_INLINE QMvec4 qm_sub_v4f(QMvec4 l, QMfloat r) {
	l.x -= r; l.y -= r; l.z -= r; l.w -= r; return l;
}
QM_INLINE QMvec4 qm_sub_v4(QMvec4 l, QMvec4 r) {
	l.x -= r.x; l.y -= r.y; l.z -= r.z; l.w -= r.w; return l;
}

/* Multiplication */
QM_INLINE QMvec2 qm_mul_v2f(QMvec2 l, QMfloat r) {
	l.x *= r; l.y *= r; return l;
}
QM_INLINE QMvec2 qm_mul_v2(QMvec2 l, QMvec2 r) {
	l.x *= r.x; l.y *= r.y; return l;
}
QM_INLINE QMvec3 qm_mul_v3f(QMvec3 l, QMfloat r) {
	l.x *= r; l.y *= r; l.z *= r; return l;
}
QM_INLINE QMvec3 qm_mul_v3(QMvec3 l, QMvec3 r) {
	l.x *= r.x; l.y *= r.y; l.z *= r.z; return l;
}
QM_INLINE QMvec4 qm_mul_v4f(QMvec4 l, QMfloat r) {
	l.x *= r; l.y *= r; l.z *= r; l.w *= r; return l;
}
QM_INLINE QMvec4 qm_mul_v4(QMvec4 l, QMvec4 r) {
	l.x *= r.x; l.y *= r.y; l.z *= r.z; l.w *= r.w; return l;
}

/* Division */
QM_INLINE QMvec2 qm_div_v2f(QMvec2 l, QMfloat r) {
	l.x /= r; l.y /= r; return l;
}
QM_INLINE QMvec2 qm_div_v2(QMvec2 l, QMvec2 r) {
	l.x /= r.x; l.y /= r.y; return l;
}
QM_INLINE QMvec3 qm_div_v3f(QMvec3 l, QMfloat r) {
	l.x /= r; l.y /= r; l.z /= r; return l;
}
QM_INLINE QMvec3 qm_div_v3(QMvec3 l, QMvec3 r) {
	l.x /= r.x; l.y /= r.y; l.z /= r.z; return l;
}
QM_INLINE QMvec4 qm_div_v4f(QMvec4 l, QMfloat r) {
	l.x /= r; l.y /= r; l.z /= r; l.w /= r; return l;
}
QM_INLINE QMvec4 qm_div_v4(QMvec4 l, QMvec4 r) {
	l.x /= r.x; l.y /= r.y; l.z /= r.z; l.w /= r.w; return l;
}

/* Equality */
QM_INLINE QMint qm_eq_v2(QMvec2 l, QMvec2 r) {
	return l.x == r.x && l.y == r.y;
}
QM_INLINE QMint qm_eq_v3(QMvec3 l, QMvec3 r) {
	return l.x == r.x && l.y == r.y && l.z == r.z;
}
QM_INLINE QMint qm_eq_v4(QMvec4 l, QMvec4 r) {
	return l.x == r.x && l.y == r.y && l.z == r.z && l.w == r.w;
}

/* Dotproduct */
QM_INLINE QMfloat qm_dot_v2(QMvec2 l, QMvec2 r) {
	return l.x * r.x + l.y * r.y;
}
QM_INLINE QMfloat qm_dot_v3(QMvec3 l, QMvec3 r) {
	return l.x * r.x + l.y * r.y + l.z * r.z;
}
QM_INLINE QMfloat qm_dot_v4(QMvec4 l, QMvec4 r) {
	return l.x * r.x + l.y * r.y + l.z * r.z + l.w * r.w;
}

/* Crossproduct */
QM_INLINE QMvec3 qm_cross_v3(QMvec3 l, QMvec3 r) {
	QMvec3 ret;
	ret.x = (l.y * r.z) - (l.z * r.y);
	ret.y = (l.z * r.x) - (l.x * r.z);
	ret.z = (l.x * r.y) - (l.y * r.x);
	return ret;
}

/**
 * Vector unary operations
 **/
/* Length */
QM_INLINE QMfloat qm_lengthsq_v2(QMvec2 v) {
	return v.x * v.x + v.y * v.y;
}
QM_INLINE QMfloat qm_lengthsq_v3(QMvec3 v) {
	return v.x * v.x + v.y * v.y + v.z * v.z;
}
QM_INLINE QMfloat qm_lengthsq_v4(QMvec4 v) {
	return v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
}

QM_INLINE QMfloat qm_length_v2(QMvec2 v) {
	return qm_sqrt(qm_lengthsq_v2(v));
}
QM_INLINE QMfloat qm_length_v3(QMvec3 v) {
	return qm_sqrt(qm_lengthsq_v3(v));
}
QM_INLINE QMfloat qm_length_v4(QMvec4 v) {
	return qm_sqrt(qm_lengthsq_v4(v));
}

/* Normalize */
QM_INLINE QMvec2 qm_normalize_v2(QMvec2 v)
{
	QMvec2 ret = {0};
	QMfloat length = qm_length_v2(v);
	if (length != 0.0f) {
		ret.x = v.x * (1.0f / length);
		ret.y = v.y * (1.0f / length);
	}
	return ret;
}
QM_INLINE QMvec3 qm_normalize_v3(QMvec3 v)
{
	QMvec3 ret = {0};
	QMfloat length = qm_length_v3(v);
	if (length != 0.0f) {
		ret.x = v.x * (1.0f / length);
		ret.y = v.y * (1.0f / length);
		ret.z = v.z * (1.0f / length);
	}
	return ret;
}
QM_INLINE QMvec4 qm_normalize_v4(QMvec4 v)
{
	QMvec4 ret = {0};
	QMfloat length = qm_length_v4(v);
	if (length != 0.0f) {
		ret.x = v.x * (1.0f / length);
		ret.y = v.y * (1.0f / length);
		ret.z = v.z * (1.0f / length);
		ret.w = v.w * (1.0f / length);
	}
	return ret;
}


/**
 * Matrix binary operations
 **/
QM_EXTERN QMmat4 qm_add_m4f(QMmat4 l, QMfloat r);
QM_EXTERN QMmat4 qm_sub_m4f(QMmat4 l, QMfloat r);
QM_EXTERN QMmat4 qm_mul_m4f(QMmat4 l, QMfloat r);
QM_EXTERN QMmat4 qm_div_m4f(QMmat4 l, QMfloat r);
QM_EXTERN QMmat4 qm_add_m4(QMmat4 l, QMmat4 r);
QM_EXTERN QMmat4 qm_sub_m4(QMmat4 l, QMmat4 r);
QM_EXTERN QMmat4 qm_mul_m4(QMmat4 l, QMmat4 r);
QM_EXTERN QMmat4 qm_div_m4(QMmat4 l, QMmat4 r);
QM_EXTERN QMmat4 qm_cross_m4(QMmat4 l, QMmat4 r);
QM_EXTERN QMvec4 qm_mul_m4v4(QMmat4 l, QMvec4 r);

/**
 * Matrix unary operations
 **/
QM_EXTERN QMmat4 qm_transpose_m4(QMmat4 m);

#define QUICK_MATH_H
#endif

#if defined(QUICK_MATH_IMPLEMENT)

/**
 * Matrix initialization
 **/
QMmat4 qm_rotate(QMfloat angle, QMvec3 axis)
{
	const QMfloat Sin = qm_sin(angle);
	const QMfloat Cos = qm_cos(angle);
	const QMfloat sub1Cos = 1.0f - Cos;

	QMmat4 ret = {0};

	ret.m00 = axis.x * axis.x * sub1Cos + Cos;
	ret.m01 = axis.x * axis.y * sub1Cos - axis.z * Sin;
	ret.m02 = axis.x * axis.z * sub1Cos + axis.y * Sin;

	ret.m10 = axis.y * axis.x * sub1Cos + axis.z * Sin;
	ret.m11 = axis.y * axis.y * sub1Cos + Cos;
	ret.m12 = axis.y * axis.z * sub1Cos - axis.x * Sin;

	ret.m20 = axis.z * axis.x * sub1Cos - axis.y * Sin;
	ret.m21 = axis.z * axis.y * sub1Cos + axis.x * Sin;
	ret.m22 = axis.z * axis.z * sub1Cos + Cos;

	ret.m33 = 1;

	return ret;
}
QMmat4 qm_lookat(QMvec3 eye, QMvec3 center, QMvec3 up)
{
	const QMvec3 f = qm_normalize_v3(qm_sub_v3(center, eye));
	const QMvec3 s = qm_normalize_v3(qm_cross_v3(f, up));
	QMvec3 u = qm_cross_v3(s, f);
	QMmat4 ret = {0};

	ret.m00 = s.x;
	ret.m01 = u.x;
	ret.m02 = -f.x;
	ret.m03 = 0.0f;

	ret.m10 = s.y;
	ret.m11 = u.y;
	ret.m12 = -f.y;
	ret.m13 = 0.0f;

	ret.m20 = s.z;
	ret.m21 = u.z;
	ret.m22 = -f.z;
	ret.m23 = 0.0f;

	ret.m30 = -qm_dot_v3(s, eye);
	ret.m31 = -qm_dot_v3(u, eye);
	ret.m32 = qm_dot_v3(f, eye);
	ret.m33 = 1.0f;

	return ret;
}

/**
 * Matrix binary operations
 **/

QMmat4 qm_add_m4f(QMmat4 l, QMfloat r) {
	l.m00 += r; l.m01 += r; l.m02 += r; l.m03 += r;
	l.m10 += r; l.m11 += r; l.m12 += r; l.m13 += r;
	l.m20 += r; l.m21 += r; l.m22 += r; l.m23 += r;
	l.m30 += r; l.m31 += r; l.m32 += r; l.m33 += r;
	return l;
}
QMmat4 qm_sub_m4f(QMmat4 l, QMfloat r) {
	l.m00 -= r; l.m01 -= r; l.m02 -= r; l.m03 -= r;
	l.m10 -= r; l.m11 -= r; l.m12 -= r; l.m13 -= r;
	l.m20 -= r; l.m21 -= r; l.m22 -= r; l.m23 -= r;
	l.m30 -= r; l.m31 -= r; l.m32 -= r; l.m33 -= r;
	return l;
}
QMmat4 qm_mul_m4f(QMmat4 l, QMfloat r) {
	l.m00 *= r; l.m01 *= r; l.m02 *= r; l.m03 *= r;
	l.m10 *= r; l.m11 *= r; l.m12 *= r; l.m13 *= r;
	l.m20 *= r; l.m21 *= r; l.m22 *= r; l.m23 *= r;
	l.m30 *= r; l.m31 *= r; l.m32 *= r; l.m33 *= r;
	return l;
}
QMmat4 qm_div_m4f(QMmat4 l, QMfloat r) {
	l.m00 /= r; l.m01 /= r; l.m02 /= r; l.m03 /= r;
	l.m10 /= r; l.m11 /= r; l.m12 /= r; l.m13 /= r;
	l.m20 /= r; l.m21 /= r; l.m22 /= r; l.m23 /= r;
	l.m30 /= r; l.m31 /= r; l.m32 /= r; l.m33 /= r;
	return l;
}
QMmat4 qm_add_m4(QMmat4 l, QMmat4 r)
{
	l.m00 += r.m00; l.m01 += r.m01; l.m02 += r.m02; l.m03 += r.m03;
	l.m10 += r.m10; l.m11 += r.m11; l.m12 += r.m12; l.m13 += r.m13;
	l.m20 += r.m20; l.m21 += r.m21; l.m22 += r.m22; l.m23 += r.m23;
	l.m30 += r.m30; l.m31 += r.m31; l.m32 += r.m32; l.m33 += r.m33;
	return l;
}
QMmat4 qm_sub_m4(QMmat4 l, QMmat4 r)
{
	l.m00 -= r.m00; l.m01 -= r.m01; l.m02 -= r.m02; l.m03 -= r.m03;
	l.m10 -= r.m10; l.m11 -= r.m11; l.m12 -= r.m12; l.m13 -= r.m13;
	l.m20 -= r.m20; l.m21 -= r.m21; l.m22 -= r.m22; l.m23 -= r.m23;
	l.m30 -= r.m30; l.m31 -= r.m31; l.m32 -= r.m32; l.m33 -= r.m33;
	return l;
}
QMmat4 qm_mul_m4(QMmat4 l, QMmat4 r)
{
	l.m00 *= r.m00; l.m01 *= r.m01; l.m02 *= r.m02; l.m03 *= r.m03;
	l.m10 *= r.m10; l.m11 *= r.m11; l.m12 *= r.m12; l.m13 *= r.m13;
	l.m20 *= r.m20; l.m21 *= r.m21; l.m22 *= r.m22; l.m23 *= r.m23;
	l.m30 *= r.m30; l.m31 *= r.m31; l.m32 *= r.m32; l.m33 *= r.m33;
	return l;
}
QMmat4 qm_div_m4(QMmat4 l, QMmat4 r)
{
	l.m00 /= r.m00; l.m01 /= r.m01; l.m02 /= r.m02; l.m03 /= r.m03;
	l.m10 /= r.m10; l.m11 /= r.m11; l.m12 /= r.m12; l.m13 /= r.m13;
	l.m20 /= r.m20; l.m21 /= r.m21; l.m22 /= r.m22; l.m23 /= r.m23;
	l.m30 /= r.m30; l.m31 /= r.m31; l.m32 /= r.m32; l.m33 /= r.m33;
	return l;
}
QMmat4 qm_cross_m4(QMmat4 l, QMmat4 r)
{
	QMmat4 ret;
	ret.m00 = l.m00 * r.m00 + l.m10 * r.m01 + l.m20 * r.m02 + l.m30 * r.m03;
	ret.m01 = l.m01 * r.m00 + l.m11 * r.m01 + l.m21 * r.m02 + l.m31 * r.m03;
	ret.m02 = l.m02 * r.m00 + l.m12 * r.m01 + l.m22 * r.m02 + l.m32 * r.m03;
	ret.m03 = l.m03 * r.m00 + l.m13 * r.m01 + l.m23 * r.m02 + l.m33 * r.m03;

	ret.m10 = l.m00 * r.m10 + l.m10 * r.m11 + l.m20 * r.m12 + l.m30 * r.m13;
	ret.m11 = l.m01 * r.m10 + l.m11 * r.m11 + l.m21 * r.m12 + l.m31 * r.m13;
	ret.m12 = l.m02 * r.m10 + l.m12 * r.m11 + l.m22 * r.m12 + l.m32 * r.m13;
	ret.m13 = l.m03 * r.m10 + l.m13 * r.m11 + l.m23 * r.m12 + l.m33 * r.m13;

	ret.m20 = l.m00 * r.m20 + l.m10 * r.m21 + l.m20 * r.m22 + l.m30 * r.m23;
	ret.m21 = l.m01 * r.m20 + l.m11 * r.m21 + l.m21 * r.m22 + l.m31 * r.m23;
	ret.m22 = l.m02 * r.m20 + l.m12 * r.m21 + l.m22 * r.m22 + l.m32 * r.m23;
	ret.m23 = l.m03 * r.m20 + l.m13 * r.m21 + l.m23 * r.m22 + l.m33 * r.m23;

	ret.m30 = l.m00 * r.m30 + l.m10 * r.m31 + l.m20 * r.m32 + l.m30 * r.m33;
	ret.m31 = l.m01 * r.m30 + l.m11 * r.m31 + l.m21 * r.m32 + l.m31 * r.m33;
	ret.m32 = l.m02 * r.m30 + l.m12 * r.m31 + l.m22 * r.m32 + l.m32 * r.m33;
	ret.m33 = l.m03 * r.m30 + l.m13 * r.m31 + l.m23 * r.m32 + l.m33 * r.m33;
	return ret;
}
QMvec4 qm_mul_m4v4(QMmat4 l, QMvec4 r)
{
	QMvec4 ret;
	ret.x = l.m00 * r.x + l.m01 * r.y + l.m02 * r.z + l.m03 * r.w;
	ret.y = l.m10 * r.x + l.m11 * r.y + l.m12 * r.z + l.m13 * r.w;
	ret.z = l.m20 * r.x + l.m21 * r.y + l.m22 * r.z + l.m23 * r.w;
	ret.w = l.m30 * r.x + l.m31 * r.y + l.m32 * r.z + l.m33 * r.w;
	return ret;
}

/**
 * Matrix unary operations
 **/
QMmat4 qm_transpose_m4(QMmat4 m)
{
	QMmat4 ret;
	ret.m00 = m.m00; ret.m01 = m.m10; ret.m02 = m.m20; ret.m03 = m.m30;
	ret.m10 = m.m01; ret.m11 = m.m11; ret.m12 = m.m21; ret.m13 = m.m31;
	ret.m20 = m.m02; ret.m21 = m.m12; ret.m22 = m.m22; ret.m23 = m.m32;
	ret.m30 = m.m03; ret.m31 = m.m13; ret.m32 = m.m23; ret.m33 = m.m33;
	return ret;
}

#undef QUICK_MATH_IMPLEMENT
#endif
